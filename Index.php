<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="Style.css" />
    <title>Apex Wiki</title>
</head>
<header>
    <nav>
        <img class='logo' src="img/non.png" alt="">
        <a href="all_legends.php">Legendes</a>
    </nav>
</header>
<body>
<div >
    <img class="banniere" src="img/banniere.jpg" alt="">
</div>
<div class="container">
        <h1 class="title_page">Accueil</h1>
        <p>Bienvenue sur le Wiki non officiel de Apex Legends</p>
        <div class="apex_desc">
            <h3 class="title_box">Apex Legends</h3>
            <p>Apex Legends est un jeu vidéo de type battle royale développé par Respawn Entertainment et édité par Electronic Arts. Il est publié en accès gratuit le 4 février 2019 sur Microsoft Windows, Nintendo switch, PlayStation 4 et Xbox One. </p>           
        </div>

        <div class="apex_desc">
            <h3 class="title_box">Dernière mises à jour</h3>
            <p>Apex Legends est un jeu vidéo de type battle royale développé par Respawn Entertainment et édité par Electronic Arts. Il est publié en accès gratuit le 4 février 2019 sur Microsoft Windows, Nintendo switch, PlayStation 4 et Xbox One. </p>           
        </div>

    </div>
</body>
</html>
<?php
    $servername = 'localhost';
    $username = 'root';
    $password = '';
        
    //On essaie de se connecter
    try{
        $conn = new PDO("mysql:host=$servername;dbname=wiki_apex", $username, $password);
        //On définit le mode d'erreur de PDO sur Exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              
    }
        
    /*On capture les exceptions si une exception est lancée et on affiche
     *les informations relatives à celle-ci*/
    catch(PDOException $e){
        echo "Erreur : " . $e->getMessage();
    }
?>