<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Style.css" />
    <title>Wiki Apex</title>
</head>
<body>
<header>
    <nav>
        <img class='logo' src="img/non.png" alt="">
        <a href="all_legends.php">Legendes</a>
    </nav>
</header>
<body>
<div >
    <img class="banniere" src="img/banniere.jpg" alt="">
</div>
    <div class="container">
        <?php
             $servername = 'localhost';
             $username = 'root';
             $password = '';
                 
             //On essaie de se connecter
             try{
                 $db = new PDO("mysql:host=$servername;dbname=wiki_apex", $username, $password);
                 //On définit le mode d'erreur de PDO sur Exception
                 $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                       
             }
                 
             /*On capture les exceptions si une exception est lancée et on affiche
              *les informations relatives à celle-ci*/
             catch(PDOException $e){
                 echo "Erreur : " . $e->getMessage();
             }
             $all_legends = $db->prepare('SELECT * FROM legends');;
             $all_legends->execute();
             $legends = $all_legends->fetchall();
            
             $html = "<div class='list'>";
             foreach($legends as $ids => $legend){
                $html .= "<div class='element'><a href='./view_legend/".$legend['name']."'>";
                $html .= "<h1>".$legend['name']."</h1>";  
                $html .= "<img class='imgCarr' src='images/carrousellegends/".$legend['img_g']."'>";
                $html .= "</a></div>";
             }
             echo $html;
        ?>
    </div>
</body>
</html>